// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import "../Styles/swiper.css"

// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";

import banner1 from "../Images/banner1.jpg";
import banner2 from "../Images/banner2.jpg";
import banner3 from "../Images/banner3.jpg";

const CarouselComp = () => {
    return (
        <>
            <Swiper
                spaceBetween={30}
                centeredSlides={true}
                autoplay={{
                    delay: 2500,
                    disableOnInteraction: false,
                }}
                pagination={{
                    clickable: true,
                }}
                navigation={true}
                modules={[Autoplay, Pagination, Navigation]}
                className="mySwiper"
            >
                <SwiperSlide><img src={banner1} alt="banner1" /></SwiperSlide>
                <SwiperSlide><img src={banner2} alt="banner2" /></SwiperSlide>
                <SwiperSlide><img src={banner3} alt="banner3" /></SwiperSlide>
            </Swiper>
        </>
    );
}

export default CarouselComp