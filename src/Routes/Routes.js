import { BrowserRouter, Route, Routes } from "react-router-dom";
import Homepage from "../Pages/Public/Homepage";

const AppRoutes = () => {


    return (
        <BrowserRouter>
            <Routes>
                {/* Public Routes */}
                <Route path="/" element={<Homepage />} />
            </Routes>
        </BrowserRouter>
    );
};

export default AppRoutes;
